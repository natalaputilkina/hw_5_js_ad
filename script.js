// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад доданий в репозиторії групи, посилання на ДЗ вище), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

async function getUsers(){
    const usersUrl = 'https://ajax.test-danit.com/api/json/users'
    try {
        const response = await fetch(usersUrl)
        const data = await response.json()
        return data
    }
    catch (error) {
        if(!error instanceof TypeError) {
            throw error
        }
    }
}


async function getPosts(){
    const usersUrl = 'https://ajax.test-danit.com/api/json/posts'
    try {
        const response = await fetch(usersUrl)
        const data = await response.json()
        return data
    }
    catch (error) {
        if(!error instanceof TypeError) {
            throw error
        }
    }
}

const conteinerPostsUsers = document.getElementById('carts-conteiner')

async function renderUsersPosts() {

    const users = await getUsers();
    const posts = await getPosts();

    posts.forEach(post => {
        const conteinerPostsUser = document.createElement('div')
        conteinerPostsUser.id = post.id
        const titlePostUser = document.createElement('h4')
        titlePostUser.textContent = post.title
        const textPostUser = document.createElement('p')
        textPostUser.textContent = post.body
        const buttonDeletePostUser = document.createElement('button')
        buttonDeletePostUser.textContent = 'delete'
        buttonDeletePostUser.id = post.id

        buttonDeletePostUser.addEventListener('click', event => {
            const deletePost = 'https://ajax.test-danit.com/api/json/posts/${postId}'

            const deletePostUser = event.target.id
            fetch(`https://ajax.test-danit.com/api/json/posts/${deletePostUser}`, {
                method: 'DELETE',
                
            })
            .then(response => {
                if(response.ok == true){
                    conteinerPostsUser.remove(deletePostUser)
                }
            })    
          
    })

        conteinerPostsUser.style.width = '1000px'
        conteinerPostsUser.style.backgroundColor = '#000000'

        titlePostUser.style.color = 'grey'
        titlePostUser.style.fontSize = '30px'

        textPostUser.style.color = '#EEE'
        textPostUser.style.fontSize = '20px'
    
    users.forEach(user => {

            if(user.id === post.userId){
                const userName = document.createElement('h2')
                userName.textContent = user.name
                const userEmail = document.createElement('h3')
                userEmail.textContent = user.email

                userName.style.color = '#EEE'
                userName.style.fontSize = '40px'

                userEmail.style.color = 'grey'
                userEmail.style.fontSize = '30px'

                conteinerPostsUsers.append(conteinerPostsUser)
                conteinerPostsUser.append(userName)
                conteinerPostsUser.append(userEmail)
                conteinerPostsUser.append(titlePostUser)
                conteinerPostsUser.append(textPostUser)
                conteinerPostsUser.append(buttonDeletePostUser)
            }

        })
       
});

}

renderUsersPosts()


